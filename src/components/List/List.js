import React, {useContext, useState} from 'react';
import {listTaskContext} from '../../contexts/listContext';
import AddItemForm from './AddItemForm';
import ListItem from './ListItem';
import {FlatList, Text} from 'react-native';

export default function List() {
  const {listTask} = useContext(listTaskContext);

  const [selectedItem, setSelectedItem] = useState(false);

  const onEdit = item => {
    setSelectedItem(item);
  };

  return (
    <>
      <AddItemForm selectedItem={selectedItem} />
      <FlatList
        data={listTask}
        keyExtractor={item => item.id}
        renderItem={({item}) => <ListItem item={item} onEdit={onEdit} />}
      />
    </>
  );
}