import React, { useState } from "react";
import { Link } from "react-router-dom";
import Button from "../components/Button";
import logo from "../logo.svg";

export default function HomeView() {
  return (
    <header className="App-header">
      <Button component={Link} to="/list" title={"Go to List"} />
    </header>
  );
}