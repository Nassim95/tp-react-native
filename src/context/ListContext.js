import React from 'react';
import Geolocation from 'react-native-geolocation-service';


  const location = Geolocation.getCurrentPosition(
    (position) => {
        return position;
    },
    { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 });

  const data = [
    {id: 1, name: 'listTask1Task1', done: false, date: new Date.now(), geoloc: location},
    {id: 2, name: 'listTask2Task2', done: false, date: new Date.now(), geoloc: location},
  ];



  export const listContext = createContext();

  export default function listProvider({children}) {
    const [listTask, setlistTask] = useState();

    useEffect(() => {
      setlistTask(data);
    }, []);

    const deleteElement = useCallback(
      item => {
        setlistTask(listTask.filter(_item => _item.id !== item.id));
      },
      [listTask],
    );

    const editElement = useCallback(
      values => {
        setlistTask(listTask.map(it => (it.id === values.id ? values : it)));
      },
      [listTask],
    );

    const addElement = useCallback(
      values => {
        setlistTask([...listTask, {id: Date.now(), ...values}]);
      },
      [listTask],
    );

    const getItem = useCallback(
      id => {
        return listTask.find(it => it.id === id);
      },
      [listTask],
    );

    return (
      <listContext.Provider
        value={{
          listTask: listTask ?? [],
          deleteElement,
          editElement,
          addElement,
          getItem,
          isReady: listTask !== undefined,
        }}>
        {children}
      </listContext.Provider>
    );
  }
